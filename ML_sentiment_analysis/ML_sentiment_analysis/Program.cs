﻿using ML_sentiment_analysisML.Model;
using System;

namespace ML_sentiment_analysis
{
    class Program
    {
        static void Main(string[] args)
        {
            // Add input data
            var input = new ModelInput();
            begin:
            Console.Write("Input your text : ");
            input.SentimentText = Console.ReadLine();
            Console.WriteLine("====================================================================\n");
            ModelOutput result = ConsumeModel.Predict(input);
            string res;
            if (result.Prediction == "1")
            {
                res = "True";
            }
            else
            {
                res = "False";
            }
            Console.WriteLine($"Text: { input.SentimentText } \nIs Toxic: "+res);
            Console.WriteLine("press any key to continue");
            Console.ReadKey();
            Console.Clear();
            goto begin;
        }
    }
}
